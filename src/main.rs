extern crate sdl2;

use sdl2::event::Event;
use sdl2::surface::Surface;
use sdl2::video::{Window, WindowContext};
use sdl2::image::INIT_PNG;
use sdl2::pixels::Color;
use sdl2::render::{TextureCreator, Texture, Canvas, BlendMode};
use sdl2::rect::{Rect, Point};
use sdl2::keyboard::Keycode;
use sdl2::ttf::{Sdl2TtfContext, Font};
use std::path::Path;

const BUTTON_WIDTH: u32 = 300;
const BUTTON_HEIGHT: u32 = 200;

fn main() {
    let sdl_context = sdl2::init().unwrap();
    let sdl_image = sdl2::image::init(INIT_PNG);
    let video_subsystem = sdl_context.video().unwrap();

    let mut window = video_subsystem
        .window("rust-sdl2 demo", 640, 480)
        .position_centered()
        .opengl()
        .build()
        .unwrap();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let mut canvas = window
        .into_canvas()
        .present_vsync()
        .accelerated()
        .build()
        .unwrap();
    let texture_creator = canvas.texture_creator();
    let width = canvas.viewport().width();
    let height = canvas.viewport().height();
    let ttf_context = sdl2::ttf::init().unwrap();

    let button_texture = LTexture::new_image("images/button.png", &texture_creator);

    let sprite_clips = [Rect::new(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT),
                        Rect::new(0, 200, BUTTON_WIDTH, BUTTON_HEIGHT),
                        Rect::new(0, 400, BUTTON_WIDTH, BUTTON_HEIGHT),
                        Rect::new(0, 600, BUTTON_WIDTH, BUTTON_HEIGHT)];
    let mut buttons = [LButton::new(Point::new(0, 0), &button_texture),
                       LButton::new(Point::new((width - BUTTON_WIDTH) as i32, 0),
                                    &button_texture),
                       LButton::new(Point::new(0, (height - BUTTON_HEIGHT) as i32),
                                    &button_texture),
                       LButton::new(Point::new((width - BUTTON_WIDTH) as i32,
                                               (height - BUTTON_HEIGHT) as i32),
                                    &button_texture)];

    canvas.present();

    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => break 'running,
                _ => {}
            }

            for b in buttons.iter_mut() {
                b.handle_event(&event);
            }
        }

        canvas.set_draw_color(Color::RGB(255, 255, 255));
        canvas.clear();

        for b in &buttons {
            b.render(&sprite_clips, &mut canvas);
        }

        canvas.present();
    }
}


struct LTexture<'a> {
    texture: Texture<'a>,
    width: u32,
    height: u32,
}

impl<'a> LTexture<'a> {
    fn new_image(file: &str, texture_creator: &'a TextureCreator<WindowContext>) -> LTexture<'a> {

        let mut surface: Surface = sdl2::image::LoadSurface::from_file(Path::new(file)).unwrap();
        let width = surface.width();
        let height = surface.height();
        surface.set_color_key(true, Color::RGB(0, 255, 255));
        let texture = texture_creator
            .create_texture_from_surface(surface)
            .unwrap();

        LTexture {
            texture,
            width,
            height,
        }
    }

    #[cfg(feature = "ttf")]
    fn new_text(font_path: &Path,
                text: &str,
                color: Color,
                ttf_context: Sdl2TtfContext,
                texture_creator: &'a TextureCreator<WindowContext>)
                -> LTexture<'a> {
        let font = ttf_context.load_font(font_path, 28).unwrap();
        let surface = font.render(text).solid(color).unwrap();
        let width = surface.width();
        let height = surface.height();

        let texture = texture_creator
            .create_texture_from_surface(surface)
            .unwrap();

        LTexture {
            texture,
            width,
            height,
        }
    }

    fn width(&self) -> u32 {
        self.width
    }

    fn height(&self) -> u32 {
        self.height
    }

    fn set_color(&mut self, color: Color) {
        self.texture.set_color_mod(color.r, color.g, color.b);
    }

    fn set_blend_mode(&mut self, blend_mode: BlendMode) {
        self.texture.set_blend_mode(blend_mode);
    }

    fn set_alpha(&mut self, alpha: u8) {
        self.texture.set_alpha_mod(alpha);
    }

    fn render<R1>(&self, x: i32, y: i32, clip: R1, canvas: &mut Canvas<Window>)
        where R1: Into<Option<Rect>>
    {
        self.render_with_rotation(x, y, clip, 0.0, None, false, false, canvas);
    }

    fn render_with_rotation<R1>(&self,
                                x: i32,
                                y: i32,
                                clip: R1,
                                angle: f64,
                                center: Option<Point>,
                                flip_horizontally: bool,
                                flip_vertically: bool,
                                canvas: &mut Canvas<Window>)
        where R1: Into<Option<Rect>>
    {
        let src_clip = clip.into();
        let width = src_clip.map_or(self.width, |r| r.width());
        let height = src_clip.map_or(self.height, |r| r.height());

        canvas.copy_ex(&self.texture,
                       src_clip,
                       Rect::new(x, y, width, height),
                       angle,
                       center,
                       flip_horizontally,
                       flip_vertically);
    }
}

#[derive(Copy,Clone)]
enum LButtonSprite {
    MOUSE_OUT,
    MOUSE_OVER,
    MOUSE_DOWN,
    MOUSE_UP,
}

impl<'a> LButtonSprite {
    fn value(&self, sprite_clips: &'a [Rect]) -> &'a Rect {
        match *self {
            LButtonSprite::MOUSE_OUT => &sprite_clips[0],
            LButtonSprite::MOUSE_OVER => &sprite_clips[1],
            LButtonSprite::MOUSE_DOWN => &sprite_clips[2],
            LButtonSprite::MOUSE_UP => &sprite_clips[3],
        }
    }
}

struct LButton<'a> {
    position: Point,
    button_sprite: LButtonSprite,
    texture: &'a LTexture<'a>,
}

impl<'a> LButton<'a> {
    fn new(position: Point, texture: &'a LTexture) -> LButton<'a> {
        LButton {
            position,
            button_sprite: LButtonSprite::MOUSE_DOWN,
            texture,
        }
    }

    fn handle_event(&mut self, event: &Event) {
        let inside = match *event {
            Event::MouseButtonDown { x, y, .. } |
            Event::MouseButtonUp { x, y, .. } |
            Event::MouseMotion { x, y, .. } => {
                match (x, y) {
                    _ if x < self.position.x || x > self.position.x + BUTTON_WIDTH as i32 => false,
                    _ if y < self.position.y || y > self.position.y + BUTTON_HEIGHT as i32 => false,
                    _ => true,
                }
            }
            _ => false,
        };

        self.button_sprite = if !inside {
            LButtonSprite::MOUSE_OUT
        } else {
            match *event {
                Event::MouseButtonDown { .. } => LButtonSprite::MOUSE_DOWN,
                Event::MouseButtonUp { .. } => LButtonSprite::MOUSE_UP,
                Event::MouseMotion { .. } => LButtonSprite::MOUSE_OVER,
                _ => self.button_sprite.clone(),
            }
        }
    }

    fn render(&self, sprite_clips: &[Rect], mut canvas: &mut Canvas<Window>) {
        self.texture
            .render(self.position.x,
                    self.position.y,
                    *self.button_sprite.value(sprite_clips),
                    &mut canvas);
    }
}
